name: Docker WebApp

on:
  workflow_run:
    workflows: [".NET WebApp Build"]
    types: [completed]
    branches: [ develop, master ]
    tags: [ 'v*.*.*', 'latest' ]
  release:
    types: [published]
  workflow_dispatch:
    inputs:
      branch:
        description: 'Branch or commit to build'
        required: true
        default: 'develop'

env:
  REGISTRY: ghcr.io
  PRIVATE_REGISTRY: khaneliman.azurecr.io
  IMAGE_NAME: ${{ github.repository }}-webapp

jobs:
  build:
    runs-on: ubuntu-latest
    permissions:
      contents: read
      packages: write
    if: ${{ github.event.workflow_run.conclusion == 'success' || github.event_name == 'release' || github.event_name == 'workflow_dispatch' }}
    steps:
      - uses: haya14busa/action-cond@v1
        id: condval
        with:
          cond: ${{ github.event_name == 'workflow_dispatch' }}
          if_true: "${{ github.event.inputs.branch }}"
          if_false: " ${{ github.ref_name }}"
      - name: Checkout repository
        uses: actions/checkout@v2
        with:
          ref: ${{ steps.condval.outputs.value }}
      # Login against a Docker registry except on PR
      # https://github.com/docker/login-action
      - name: Log into registry ${{ env.REGISTRY }}
        if: github.event_name != 'pull_request'
        uses: docker/login-action@f054a8b539a109f9f41c372932f1ae047eff08c9
        with:
          registry: ${{ env.REGISTRY }}
          username: ${{ github.actor }}
          password: ${{ secrets.GITHUB_TOKEN }}

      #Login against a Docker registry except on PR
      # https://github.com/docker/login-action
      - name: Log into registry ${{ env.PRIVATE_REGISTRY }}
        if: github.event_name != 'pull_request'
        uses: docker/login-action@f054a8b539a109f9f41c372932f1ae047eff08c9
        with:
          registry: ${{ env.PRIVATE_REGISTRY }}
          username: ${{ secrets.AZURECR_USER }}
          password: ${{ secrets.AZURECR_PASS }}

      # Extract metadata (tags, labels) for Docker
      # https://github.com/docker/metadata-action
      - name: Extract Docker metadata
        id: meta
        uses: docker/metadata-action@e5622373a38e60fb6d795a4421e56882f2d7a681
        with:
          images: |
            ${{ env.REGISTRY }}/${{ env.IMAGE_NAME }}
            ${{ env.PRIVATE_REGISTRY }}/${{ env.IMAGE_NAME }}

      # GitHub Action to install QEMU static binaries.
      # https://github.com/docker/setup-qemu-action
      - name: Set up QEMU
        uses: docker/setup-qemu-action@v1.2.0

      # GitHub Action to set up Docker Buildx.
      # https://github.com/docker/setup-buildx-action
      - name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v1.6.0
          
      # Build and push Docker image with Buildx (don't push on PR)
      # https://github.com/docker/build-push-action
      - name: Build and push Docker image
        uses: docker/build-push-action@v2.7.0
        with:
          context: WebApp/
          push: ${{ github.event_name != 'pull_request' }}
          tags: ${{ steps.meta.outputs.tags }}
          labels: ${{ steps.meta.outputs.labels }}
          platforms: linux/amd64,linux/arm64
